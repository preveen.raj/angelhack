/*  'use strict';

process.env.DEBUG = 'actions-on-google:*';

const DialogflowApp = require('actions-on-google').DialogflowApp;
const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);



function getDetails(id, value, ref)
{
    const data = value;
    const sensor = id;
    return {
        status: data.status,
        start: data.start
    };
}


const database = admin.database().ref('orders/');
const countRef = database.child('status');
const maxRef = database.child('max');

exports.fetchCount = functions.https.onRequest((request,response)=>{
    console.log('headers: ' + JSON.stringify(request.headers));
    console.log('body: ' + JSON.stringify(request.body));

    const assistant = new DialogflowApp({request: request, response: response});

    let actionMap = new Map();
    actionMap.set('favorite color', rush_check);
    assistant.handleRequest(actionMap);


    function rush_check(assistant){
        console.log('inside countFunc');
		
		const max = 4;

        countRef.once('value',snap=>{
            const carCount = snap.val();
			const currentCarValue = max - carCount;
            console.log(`currentCarValue: ${currentCarValue}`);
			
			var statuspeech;
			switch(carCount){
				case 1:
				
				case 0: statuspeech = `Very Unlikely to get a parking spot`;
						break;
				case 4:
				
				case 3: statuspeech = `Very likely to get a parking spot`;
										break;
				
				case 2: statuspeech = `Hurry to get a parking spot`;
										break;
			}

            //const statuspeech = `
            //No of cars is  ${snap.val()}
            //`;

            assistant.ask(statuspeech)

        });

    }
});



 */

































 // Copyright 2018, Google, Inc.
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

'use strict';


// Import the Dialogflow module from the Actions on Google client library.
const { dialogflow,
    BasicCard,
    BrowseCarousel,
    BrowseCarouselItem,
    Button,
    Carousel,
    Image,
    LinkOutSuggestion,
    List,
    MediaObject,
    Suggestions,
    SimpleResponse,} = require('actions-on-google');

// Import the firebase-functions package for deployment.
const functions = require('firebase-functions');


const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);



// Instantiate the Dialogflow client.
const app = dialogflow({debug: true});



// Handle the Dialogflow intent named 'favorite color'.
// The intent collects a parameter named 'color'.
app.intent('get status', (conv) => {
    var status;
    var msgref = admin.database().ref("/orders/status");

   return msgref.once("value").then((snapshot)=>{
         status=snapshot.val();
         console.log("The status is: " + status);
         
         var statuspeech;
         switch(status){
            case 0:statuspeech = 'Order Failed';
            break;
            
            case 1: statuspeech = `Order Confirmed`;
                    break;
            case 2:
            
            case 3: statuspeech = `Order is Packing...`;
                                    break;
            
            case 4: statuspeech = `Order out for pickup`;
                                    break;
        }



         
         conv.ask("<speak>well..."+ "<break time='3' />" +" Yeah.. Your status is " + statuspeech + "</speak>");
    
     });
       
    
     return




});




const colorMap = {
    'Kashmiri Apple': new BasicCard({
      title: 'Kashmiri Apple',
      image: {
        url: 'https://www.apple.com/ac/structured-data/images/knowledge_graph_logo.png?201606271147',
        accessibilityText: 'Kashmiri Apple',
    },
    display: 'WHITE',
  }),
};



app.intent('show items', (conv) => {
    var status;
    var msgref = admin.database().ref("/Products");

   return msgref.once("value").then((snapshot)=>{
         status=snapshot.val();
         console.log("The status is: " + status);
         
       
         conv.close(`Here's the item`, colorMap['Kashmiri Apple']);


    
    
     });
       
    
     return




});




app.intent('show shops', (conv) => {
    
    if (!conv.surface.capabilities.has('actions.capability.SCREEN_OUTPUT')) {
        conv.ask('Sorry, try this on a screen device or select the ' +
          'phone surface in the simulator.');
        return;
      }
  
      
      // Create a list
conv.ask(new List({
    title: 'List Title',
    items: {
      // Add the first item to the list
      [SELECTION_KEY_ONE]: {
        synonyms: [
          'synonym of title 1',
          'synonym of title 2',
          'synonym of title 3',
        ],
        title: 'Title of First List Item',
        description: 'This is a description of a list item.',
        image: new Image({
          url: IMG_URL_AOG,
          alt: 'Image alternate text',
        }),
      },
      // Add the second item to the list
      [SELECTION_KEY_GOOGLE_HOME]: {
        synonyms: [
          'Google Home Assistant',
          'Assistant on the Google Home',
      ],
        title: 'Google Home',
        description: 'Google Home is a voice-activated speaker powered by ' +
          'the Google Assistant.',
        image: new Image({
          url: IMG_URL_GOOGLE_HOME,
          alt: 'Google Home',
        }),
      },
      // Add the third item to the list
      [SELECTION_KEY_GOOGLE_PIXEL]: {
        synonyms: [
          'Google Pixel XL',
          'Pixel',
          'Pixel XL',
        ],
        title: 'Google Pixel',
        description: 'Pixel. Phone by Google.',
        image: new Image({
          url: IMG_URL_GOOGLE_PIXEL,
          alt: 'Google Pixel',
        }),
      },
    },
  }));




     return;




});

const SELECTED_ITEM_RESPONSES = {
    [SELECTION_KEY_ONE]: 'You selected the first item',
    [SELECTION_KEY_GOOGLE_HOME]: 'You selected the Google Home!',
    [SELECTION_KEY_GOOGLE_PIXEL]: 'You selected the Google Pixel!',
  };


  
  app.intent('actions.intent.OPTION', (conv, params, option) => {
    let response = 'You did not select any item';
    if (option && SELECTED_ITEM_RESPONSES.hasOwnProperty(option)) {
      response = SELECTED_ITEM_RESPONSES[option];
    }
    conv.ask(response);
  });













// Set the DialogflowApp object to handle the HTTPS POST request.
exports.dialogflowFirebaseFulfillment = functions.https.onRequest(app);


 
 



/* 
// Copyright 2018, Google, Inc.
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

'use strict';


// Import the Dialogflow module from the Actions on Google client library.
const {dialogflow} = require('actions-on-google');

// Import the firebase-functions package for deployment.
const functions = require('firebase-functions');



// Instantiate the Dialogflow client.
const app = dialogflow({debug: true});

// Handle the Dialogflow intent named 'favorite color'.
// The intent collects a parameter named 'color'.
app.intent('favorite color', (conv, {color}) => {
    const luckyNumber = color.length;
    // Respond with the user's lucky number and end the conversation.
    conv.ask('Your lucky number is ' + luckyNumber);
});

// Set the DialogflowApp object to handle the HTTPS POST request.
exports.dialogflowFirebaseFulfillment = functions.https.onRequest(app);

 */
